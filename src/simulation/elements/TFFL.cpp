#include "simulation/Elements.h"
//#TPT-Directive ElementClass Element_TFFL PT_TFFL 199
Element_TFFL::Element_TFFL()
{
	Identifier = "DEFAULT_PT_TFFL";
	Name = "TFFL";
	Colour = PIXPACK(0xDD8911);
	MenuVisible = 1;
	MenuSection = SC_ELEC;
	Enabled = 1;

	Advection = 0.0f;
	AirDrag = 0.00f * CFDS;
	AirLoss = 0.90f;
	Loss = 0.00f;
	Collision = 0.0f;
	Gravity = 0.0f;
	Diffusion = 0.00f;
	HotAir = 0.000f	* CFDS;
	Falldown = 0;

	Flammable = 0;
	Explosive = 0;
	Meltable = 0;
	Hardness = 0;

	Weight = 100;

	Temperature = R_TEMP+0.0f +273.15f;
	HeatConduct = 255;
	Description = "TFFL, T-Flip Flop. PSCN to switch, NSCN to take";

	Properties = TYPE_SOLID|PROP_LIFE_DEC;

	LowPressure = IPL;
	LowPressureTransition = NT;
	HighPressure = IPH;
	HighPressureTransition = NT;
	LowTemperature = ITL;
	LowTemperatureTransition = NT;
	HighTemperature = ITH;
	HighTemperatureTransition = NT;

	Update = &Element_TFFL::update;
}

//#TPT-Directive ElementHeader Element_TFFL static int update(UPDATE_FUNC_ARGS)
int Element_TFFL::update(UPDATE_FUNC_ARGS)
{
	int ry,rx,r;
	for (rx=-2; rx<3; rx++)
			for (ry=-2; ry<3; ry++)
				if (BOUNDS_CHECK && (rx || ry))
				{
					r = pmap[y+ry][x+rx];
					if (!r)
						continue;
					if (parts[i].life == 1 && (r&0xFF) == PT_NSCN && parts[i].pavg[0]==1)
					{
						parts[r>>8].life = 4;
						parts[r>>8].ctype = PT_NSCN;
						sim->part_change_type(r>>8,x+rx,y+ry,PT_SPRK);
					}
					if ((r&0xFF) == PT_SPRK && parts[r>>8].life == 3)
					{
						if (parts[r>>8].ctype == PT_PSCN && parts[i].pavg[0]==0)
						{
							parts[i].pavg[0]=1;
							parts[i].life=2;
						}
						else if (parts[r>>8].ctype == PT_PSCN && parts[i].pavg[0]==1 && parts[i].life==0)
						{
							parts[i].pavg[0]=0;
						}
					}
				}
	return 0;
}
Element_TFFL::~Element_TFFL() {}
