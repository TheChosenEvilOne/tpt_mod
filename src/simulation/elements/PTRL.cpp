#include "simulation/Elements.h"

//#TPT-Directive ElementClass Element_PTRL PT_PTRL 184
Element_PTRL::Element_PTRL()
{
	Identifier = "DEFAULT_PT_PTRL";
	Name = "PTRL";
	Colour = PIXPACK(0x0A4A5C);
	MenuVisible = 1;
	MenuSection = SC_LIQUID;
	Enabled = 1;

	Advection = 0.8f;
	AirDrag = 0.02f * CFDS;
	AirLoss = 0.94f;
	Loss = 0.80f;
	Collision = 0.1f;
	Gravity = 0.6f;
	Diffusion = 0.00f;
	HotAir = -0.001f	* CFDS;
	Falldown = 2;

	Flammable = 0;
	Explosive = 0;
	Meltable = 0;
	Hardness = 0;

	Weight = 12;

	Temperature = R_TEMP+0.0f	+273.15f;
	HeatConduct = 75;
	Description = "Petrol. Highly flammable liquid.";

	Properties = TYPE_LIQUID;

	LowPressure = IPL;
	LowPressureTransition = NT;
	HighPressure = IPH;
	HighPressureTransition = NT;
	LowTemperature = 225.15;
	LowTemperatureTransition = PT_ICEI;
	HighTemperature = 334.15;
	HighTemperatureTransition = PT_PTVL;

	Update = &Element_PTRL::update;
}

//#TPT-Directive ElementHeader Element_PTRL static int update(UPDATE_FUNC_ARGS)
int Element_PTRL::update(UPDATE_FUNC_ARGS) 
{
	int cal,reaction,ry,rx,tx,ty;
	
	ry = rand() % 3 - 1;
	rx = rand() % 3 - 1;
	ty = rand() % 3 - 1;
	tx = rand() % 3 - 1;

	if (BOUNDS_CHECK && (rx || ry))
	{
		cal = pmap[y+ry][x+rx];
		reaction = sim->pmap[y+ty][x+tx];
		if ((cal&0xFF) == PT_ACEL && (reaction&0xFF) == PT_KERO) 
		{
			sim->part_change_type(i,x,y,PT_JETB);
		}	
	}
	return 0;
}

Element_PTRL::~Element_PTRL() {}
