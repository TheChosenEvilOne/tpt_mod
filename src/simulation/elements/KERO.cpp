#include "simulation/Elements.h"

//#TPT-Directive ElementClass Element_KERO PT_KERO 188
Element_KERO::Element_KERO()
{
	Identifier = "DEFAULT_PT_KERO";
	Name = "KERO";
	Colour = PIXPACK(0x01B3D7);
	MenuVisible = 1;
	MenuSection = SC_GAS;
	Enabled = 1;

	Advection = 0.8f;
	AirDrag = 0.01f * CFDS;
	AirLoss = 0.90f;
	Loss = 1.0f;
	Collision = 0.1f;
	Gravity = 0.04f;
	Diffusion = 0.00f;
	HotAir = -0.001f	* CFDS;
	Falldown = 2;

	Flammable = 0;
	Explosive = 0;
	Meltable = 0;
	Hardness = 0;

	Weight = 5;

	Temperature = R_TEMP+0.0f	+273.15f;
	HeatConduct = 75;
	Description = "KERO BURNS HOT AND QUICK.";

	Properties = TYPE_LIQUID|PROP_DEADLY;

	LowPressure = IPL;
	LowPressureTransition = NT;
	HighPressure = IPH;
	HighPressureTransition = NT;
	LowTemperature = ITL;
	LowTemperatureTransition = NT;
	HighTemperature = ITH;
	HighTemperatureTransition = NT;

	Update = &Element_KERO::update;
}

//#TPT-Directive ElementHeader Element_KERO static int update(UPDATE_FUNC_ARGS)
int Element_KERO::update(UPDATE_FUNC_ARGS) 
{
	int cal,reaction,ry,rx,tx,ty;
	
	ry = rand() % 3 - 1;
	rx = rand() % 3 - 1;
	ty = rand() % 3 - 1;
	tx = rand() % 3 - 1;

	if (BOUNDS_CHECK && (rx || ry))
	{
		cal = pmap[y+ry][x+rx];
		reaction = sim->pmap[y+ty][x+tx];
		if ((cal&0xFF) == PT_ACEL && (reaction&0xFF) == PT_PTRL) 
		{
			sim->part_change_type(i,x,y,PT_JETB);
		}	
	}
	return 0;
}

Element_KERO::~Element_KERO() {}
