#include "simulation/Elements.h"
//#TPT-Directive ElementClass Element_LN2O PT_LN2O 195
Element_LN2O::Element_LN2O()
{
	Identifier = "DEFAULT_PT_LN2O";
	Name = "LN2O";
	Colour = PIXPACK(0x33C1F5);
	MenuVisible = 1;
	MenuSection = SC_LIQUID;
	Enabled = 1;

	Advection = 0.7f;
	AirDrag = 0.03f * CFDS;
	AirLoss = 0.94f;
	Loss = 0.5f;
	Collision = 0.0f;
	Gravity = 0.8f;
	Diffusion = 0.00f;
	HotAir = 0.000f  * CFDS;
	Falldown = 2;

	Flammable = 6000;
	Explosive = 0;
	Meltable = 0;
	Hardness = 20;

	Weight = 43;

	Temperature = R_TEMP+174.15f;
	HeatConduct = 58;
	Description = "Liquid Nitrous Oxide. Burns very quickly and very hot.";

	Properties = TYPE_LIQUID;

	LowPressure = IPL;
	LowPressureTransition = NT;
	HighPressure = IPH;
	HighPressureTransition = NT;
	LowTemperature = 160.15;
	LowTemperatureTransition = PT_ICEI;
	HighTemperature = 185;
	HighTemperatureTransition = PT_LAVA;

	Update = NULL;
}

Element_LN2O::~Element_LN2O() {}
