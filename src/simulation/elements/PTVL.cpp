#include "simulation/Elements.h"
//#TPT-Directive ElementClass Element_PTVL PT_PTVL 186
Element_PTVL::Element_PTVL()
{
	Identifier = "DEFAULT_PT_PTVL";
	Name = "PTVL";
	Colour = PIXPACK(0x0A4A5C);
	MenuVisible = 1;
	MenuSection = SC_GAS;
	Enabled = 1;

	Advection = 1.0f;
	AirDrag = 0.01f * CFDS;
	AirLoss = 0.97f;
	Loss = 0.75f;
	Collision = 0.1f;
	Gravity = -0.02f;
	Diffusion = 0.7f;
	HotAir = -0.001f	* CFDS;
	Falldown = 2;

	Flammable = 500;
	Explosive = 1;
	Meltable = 0;
	Hardness = 0;

	Weight = 0;

	Temperature = R_TEMP+343.15f;
	HeatConduct = 75;
	Description = "Petrol gas.";

	Properties = TYPE_GAS;

	LowPressure = IPL;
	LowPressureTransition = NT;
	HighPressure = IPH;
	HighPressureTransition = NT;
	LowTemperature = 333.15;
	LowTemperatureTransition = PT_PTRL;
	HighTemperature = ITL;
	HighTemperatureTransition = NT;
}

Element_PTVL::~Element_PTVL() {}
