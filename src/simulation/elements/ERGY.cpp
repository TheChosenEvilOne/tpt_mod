#include "simulation/Elements.h"
//#TPT-Directive ElementClass Element_ERGY PT_ERGY 185
Element_ERGY::Element_ERGY()
{
	Identifier = "DEFAULT_PT_ERGY";
	Name = "ERGY";
	Colour = PIXPACK(0x242424);
	MenuVisible = 1;
	MenuSection = SC_NUCLEAR;
	Enabled = 1;

	Advection = 0.0f;
	AirDrag = 0.00f * CFDS;
	AirLoss = 1.00f;
	Loss = 1.00f;
	Collision = -.99f;
	Gravity = 0.0f;
	Diffusion = 0.00f;
	HotAir = 0.000f	* CFDS;
	Falldown = 0;

	Flammable = 0;
	Explosive = 0;
	Meltable = 0;
	Hardness = 0;

	Weight = -1;

	Temperature = R_TEMP+0.0f	+273.15f;
	HeatConduct = 70;
	Description = "Pure Energy, Extremely Powerful";

	Properties = TYPE_ENERGY;

	LowPressure = IPL;
	LowPressureTransition = NT;
	HighPressure = IPH;
	HighPressureTransition = NT;
	LowTemperature = ITL;
	LowTemperatureTransition = NT;
	HighTemperature = ITH;
	HighTemperatureTransition = NT;

	Update = &Element_ERGY::update;
}

//#TPT-Directive ElementHeader Element_ERGY static int update(UPDATE_FUNC_ARGS)
int Element_ERGY::update(UPDATE_FUNC_ARGS)
{	
	int rx,ry,r;
	rx = x+rand()%3-1;
	ry = y+rand()%3-1;
	r = pmap[ry][rx];
	if (r)
		return 0;
	else {
		for (int d = 1; d < 4; d++) {
			switch (rand() % 4 + 1) {
			case 1:
				sim->create_part(-1, rx, ry, PT_PHOT);
			case 2:
				sim->create_part(-1, rx, ry, PT_NEUT);
				sim->create_part(-1, rx, ry, PT_PROT);
			case 3:
				sim->create_part(-1, rx, ry, PT_ELEC);
			case 4:
				sim->gravmap[(y / CELL)*(XRES / CELL) + (x / CELL)] = (rand() % 201 - 100);
			}
		}
	}
	parts[i].temp = 999999;
	sim->pv[(y/CELL)][(x/CELL)] += 1256;
	return 0;
}


Element_ERGY::~Element_ERGY() {}
