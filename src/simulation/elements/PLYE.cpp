#include "simulation/Elements.h"
//#TPT-Directive ElementClass Element_PLYE PT_PLYE 194
Element_PLYE::Element_PLYE()
{
	Identifier = "DEFAULT_PT_PLYE";
	Name = "PLYE";
	Colour = PIXPACK(0xCCCCCC);
	MenuVisible = 1;
	MenuSection = SC_POWDERS;
	Enabled = 1;

	Advection = 0.4f;
	AirDrag = 0.03f * CFDS;
	AirLoss = 0.96f;
	Loss = 0.15f;
	Collision = -0.1f;
	Gravity = 0.33f;
	Diffusion = 0.00f;
	HotAir = 0.000f	* CFDS;
	Falldown = 1;

	Flammable = 0;
	Explosive = 0;
	Meltable = 0;
	Hardness = 0;

	Weight = 60;

	Temperature = R_TEMP+0.0f	+273.15f;
	HeatConduct = 64;
	Description = "Potash Lye.";

	Properties = TYPE_PART|PROP_DEADLY;

	LowPressure = IPL;
	LowPressureTransition = NT;
	HighPressure = IPH;
	HighPressureTransition = NT;
	LowTemperature = ITL;
	LowTemperatureTransition = NT;
	HighTemperature = 679.15f;
	HighTemperatureTransition = PT_LAVA;

	Update = &Element_PLYE::update;
	Graphics = &Element_COAL::graphics;
}


//#TPT-Directive ElementHeader Element_PLYE static int update(UPDATE_FUNC_ARGS)
int Element_PLYE::update(UPDATE_FUNC_ARGS)
{
	int rt;
	rt = (sim->pmap[y+rand() % 3 - 1][x+rand() % 3 - 1]&0xFF);
	if (rt == PT_ACID)
		sim->part_change_type(i,x,y,PT_SLTW);
	return 0;
}

Element_PLYE::~Element_PLYE() {}
