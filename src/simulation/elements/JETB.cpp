#include "simulation/Elements.h"
//#TPT-Directive ElementClass Element_JETB PT_JETB 189
Element_JETB::Element_JETB()
{
	Identifier = "DEFAULT_PT_JETB";
	Name = "JETB";
	Colour = PIXPACK(0x01B3D7);
	MenuVisible = 1;
	MenuSection = SC_GAS;
	Enabled = 1;

	Advection = 0.8f;
	AirDrag = 0.01f * CFDS;
	AirLoss = 0.90f;
	Loss = 1.0f;
	Collision = 0.1f;
	Gravity = 0.04f;
	Diffusion = 0.00f;
	HotAir = -0.001f	* CFDS;
	Falldown = 2;

	Flammable = 0;
	Explosive = 0;
	Meltable = 0;
	Hardness = 0;

	Weight = 5;

	Temperature = R_TEMP+0.0f	+273.15f;
	HeatConduct = 75;
	Description = "KERO BURNS HOT AND QUICK.";

	Properties = TYPE_LIQUID|PROP_DEADLY;

	LowPressure = IPL;
	LowPressureTransition = NT;
	HighPressure = IPH;
	HighPressureTransition = NT;
	LowTemperature = 225.15;
	LowTemperatureTransition = PT_ICEI;
	HighTemperature = 333.15;
	HighTemperatureTransition = PT_PTVL;
}
Element_JETB::~Element_JETB() {}
