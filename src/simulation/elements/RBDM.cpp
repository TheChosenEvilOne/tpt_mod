#include "simulation/Elements.h"
//#TPT-Directive ElementClass Element_RBDM PT_RBDM 41
Element_RBDM::Element_RBDM()
{
	Identifier = "DEFAULT_PT_RBDM";
	Name = "RBDM";
	Colour = PIXPACK(0xCCCCCC);
	MenuVisible = 1;
	MenuSection = SC_EXPLOSIVE;
	Enabled = 1;

	Advection = 0.0f;
	AirDrag = 0.00f * CFDS;
	AirLoss = 0.90f;
	Loss = 0.00f;
	Collision = 0.0f;
	Gravity = 0.0f;
	Diffusion = 0.00f;
	HotAir = 0.000f	* CFDS;
	Falldown = 0;

	Flammable = 1000;
	Explosive = 1;
	Meltable = 50;
	Hardness = 1;

	Weight = 100;

	Temperature = R_TEMP+0.0f	+273.15f;
	HeatConduct = 240;
	Description = "Rubidium. Explosive, especially on contact with water. Low melting point.";

	Properties = TYPE_SOLID|PROP_CONDUCTS|PROP_LIFE_DEC;

	LowPressure = IPL;
	LowPressureTransition = NT;
	HighPressure = IPH;
	HighPressureTransition = NT;
	LowTemperature = ITL;
	LowTemperatureTransition = NT;
	HighTemperature = 312.0f;
	HighTemperatureTransition = PT_LRBD;

	Update = &Element_RBDM::update;
}

//#TPT-Directive ElementHeader Element_RBDM static int update(UPDATE_FUNC_ARGS)
int Element_RBDM::update(UPDATE_FUNC_ARGS) 
{
	if (sim->pmap[y+rand() % 3 - 1][x+rand() % 3 - 1]&0xFF == PT_ACID)
		sim->part_change_type(i,x,y,PT_COAL);
	return 0;
}

Element_RBDM::~Element_RBDM() {}
