#include "simulation/Elements.h"

//#TPT-Directive ElementClass Element_EWHM PT_EWHM 197
Element_EWHM::Element_EWHM()
{
	Identifier = "DEFAULT_PT_EWHM";
	Name = "EWHM";
	Colour = PIXPACK(0xF7A989);
	MenuVisible = 1;
	MenuSection = SC_SOLIDS;
	Enabled = 1;

	Advection = 0.0f;
	AirDrag = 0.00f * CFDS;
	AirLoss = 0.90f;
	Loss = 0.00f;
	Collision = 0.0f;
	Gravity = 0.0f;
	Diffusion = 0.00f;
	HotAir = 0.000f	* CFDS;
	Falldown = 0;

	Flammable = 0;
	Explosive = 0;
	Meltable = 1;
	Hardness = 50;

	Weight = 100;

	Temperature = R_TEMP+0.0f +273.15f;
	HeatConduct = 251;
	Description = "E180 With High Melting point, see E180";

	Properties = TYPE_SOLID|PROP_CONDUCTS|PROP_HOT_GLOW|PROP_LIFE_DEC;

	LowPressure = IPL;
	LowPressureTransition = NT;
	HighPressure = IPH;
	HighPressureTransition = NT;
	LowTemperature = ITL;
	LowTemperatureTransition = NT;
	HighTemperature = 9356.15f;
	HighTemperatureTransition = PT_LAVA;

	Update = &Element_E180::update;
}

Element_EWHM::~Element_EWHM() {}
