#include "simulation/Elements.h"
//#TPT-Directive ElementClass Element_DSLV PT_DSLV 190
Element_DSLV::Element_DSLV()
{
	Identifier = "DEFAULT_PT_DSLV";
	Name = "DSLV";
	Colour = PIXPACK(0x440000);
	MenuVisible = 0;
	MenuSection = SC_GAS;
	Enabled = 1;

	Advection = 1.0f;
	AirDrag = 0.01f * CFDS;
	AirLoss = 0.95f;
	Loss = 0.75f;
	Collision = 0.1f;
	Gravity = -0.02f;
	Diffusion = 0.7f;
	HotAir = -0.001f	* CFDS;
	Falldown = 0;

	Flammable = 550;
	Explosive = 1;
	Meltable = 0;
	Hardness = 0;

	Weight = 0;

	Temperature = R_TEMP+483.15;
	HeatConduct = 80;
	Description = "Diesel Gas.";

	Properties = TYPE_GAS;

	LowPressure = IPL;
	LowPressureTransition = NT;
	HighPressure = IPH;
	HighPressureTransition = NT;
	LowTemperature = 473.15;
	LowTemperatureTransition = PT_DESL;
	HighTemperature = ITH;
	HighTemperatureTransition = NT;
}
Element_DSLV::~Element_DSLV() {}
