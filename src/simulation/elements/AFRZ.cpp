#include "simulation/Elements.h"
//#TPT-Directive ElementClass Element_AFRZ PT_AFRZ 187
Element_AFRZ::Element_AFRZ()
{
	Identifier = "DEFAULT_PT_AFRZ";
	Name = "AFRZ";
	Colour = PIXPACK(0x0C3F5B);
	MenuVisible = 1;
	MenuSection = SC_LIQUID;
	Enabled = 1;

	Advection = 0.8f;
	AirDrag = 0.02f * CFDS;
	AirLoss = 0.98f;
	Loss = 0.8f;
	Collision = 0.1f;
	Gravity = 0.6f;
	Diffusion = 0.7f;
	HotAir = -0.001f	* CFDS;
	Falldown = 2;

	Flammable = 65;
	Explosive = 0;
	Meltable = 0;
	Hardness = 0;

	Weight = 12;

	Temperature = R_TEMP+295.15f;
	HeatConduct = 78;
	Description = "Antifreeze. Keeps your fuel nice and toasty. methnol";

	Properties = TYPE_LIQUID|PROP_DEADLY;

	LowPressure = IPL;
	LowPressureTransition = NT;
	HighPressure = IPH;
	HighPressureTransition = NT;
	LowTemperature = ITL;
	LowTemperatureTransition = NT;
	HighTemperature = ITH;
	HighTemperatureTransition = NT;
	
	Update = &Element_AFRZ::update;
}

//#TPT-Directive ElementHeader Element_AFRZ static int update(UPDATE_FUNC_ARGS)
int Element_AFRZ::update(UPDATE_FUNC_ARGS) {

	int warmId = sim->pmap[x+rand()%3-1] [y+rand()%3-1]>>8;
	int warm = parts[warmId].type;
	int warm2 = parts[i].temp;

	if (warm == PT_SPRK || warm == PT_LIGH)
	{
		sim->part_change_type(i,x,y, PT_FIRE);
	}
	else if (warm2 < 276.15f) 
	{
		if (warm == PT_PTRL) 
		{
			parts[i].temp = 296.15f;
		} 
		else if (warm == PT_DESL) 
		{
			parts[i].temp = 296.15f;
		} 
		 else if (warm == PT_ICEI) 
		{
			parts[i].temp = 296.15f;
		}
	} 
	if (warm == PT_WTRV && warm > 1100.15f) 
	{
		sim->part_change_type(i,x,y,PT_H2);
	}
	
	return 0;
}

Element_AFRZ::~Element_AFRZ() {}
