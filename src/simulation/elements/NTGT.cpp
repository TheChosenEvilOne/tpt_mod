#include "simulation/Elements.h"
//#TPT-Directive ElementClass Element_NTGT PT_NTGT 200
Element_NTGT::Element_NTGT()
{
	Identifier = "DEFAULT_PT_NTGT";
	Name = "NTGT";
	Colour = PIXPACK(0xC0FF33);
	MenuVisible = 1;
	MenuSection = SC_ELEC;
	Enabled = 1;

	Advection = 0.0f;
	AirDrag = 0.00f * CFDS;
	AirLoss = 0.90f;
	Loss = 0.00f;
	Collision = 0.0f;
	Gravity = 0.0f;
	Diffusion = 0.00f;
	HotAir = 0.000f	* CFDS;
	Falldown = 0;

	Flammable = 0;
	Explosive = 0;
	Meltable = 0;
	Hardness = 0;

	Weight = 100;

	Temperature = R_TEMP+0.0f +273.15f;
	HeatConduct = 255;
	Description = "NTGT, NoT GaTe. PSCN input, NSCN output";

	Properties = TYPE_SOLID|PROP_LIFE_DEC;

	LowPressure = IPL;
	LowPressureTransition = NT;
	HighPressure = IPH;
	HighPressureTransition = NT;
	LowTemperature = ITL;
	LowTemperatureTransition = NT;
	HighTemperature = ITH;
	HighTemperatureTransition = NT;

	Update = &Element_NTGT::update;
}

//#TPT-Directive ElementHeader Element_NTGT static int update(UPDATE_FUNC_ARGS)
int Element_NTGT::update(UPDATE_FUNC_ARGS)
{
	int ry,rx,r;
	for (rx=-2; rx<3; rx++)
			for (ry=-2; ry<3; ry++)
				if (BOUNDS_CHECK && (rx || ry))
				{
					r = pmap[y+ry][x+rx];
					if (!r)
						continue;
					if((r&0xFF) == PT_NSCN && parts[i].life == 0)
					{
						parts[r>>8].life = 4;
						parts[r>>8].ctype = PT_NSCN;
						sim->part_change_type(r>>8,x+rx,y+ry,PT_SPRK);	
					}
					if((r&0xFF) == PT_SPRK && parts[r>>8].ctype == PT_PSCN)
					{
						parts[i].life = 6;
					}
				}
	return 0;
}
Element_NTGT::~Element_NTGT() {}
