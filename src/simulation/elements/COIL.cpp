#include "simulation/Elements.h"
//#TPT-Directive ElementClass Element_COIL PT_COIL 191
Element_COIL::Element_COIL()
{
	Identifier = "DEFAULT_PT_COIL";
	Name = "COIL";
	Colour = PIXPACK(0xA57214);
	MenuVisible = 1;
	MenuSection = SC_LIQUID;
	Enabled = 1;

	Advection = 0.9f;
	AirDrag = 0.02f * CFDS;
	AirLoss = 0.91f;
	Loss = 1.0f;
	Collision = 0.1f;
	Gravity = 0.6f;
	Diffusion = 0.00f;
	HotAir = -0.001f	* CFDS;
	Falldown = 2;

	Flammable = 4;
	Explosive = 0;
	Meltable = 0;
	Hardness = 86;

	Weight = 10;

	Temperature = R_TEMP+ 10.f + 273.15f;
	HeatConduct = 100;
	Description = "Cooking oil. Bubbles when heated.";

	Properties = TYPE_LIQUID;

	LowPressure = IPL;
	LowPressureTransition = NT;
	HighPressure = IPH;
	HighPressureTransition = NT;
	LowTemperature = ITL;
	LowTemperatureTransition = NT;
	HighTemperature = ITH;
	HighTemperatureTransition = NT;

	Update = &Element_COIL::update;
}

//#TPT-Directive ElementHeader Element_COIL static int update(UPDATE_FUNC_ARGS)
int Element_COIL::update(UPDATE_FUNC_ARGS) 
{
	int ry,rx,tempe,bio,bioo;
	rx = rand() % 7 - 3;	
	ry = rand() % 7 - 5;
	tempe = parts[i].temp;
	bio = (sim->pmap[rand() % 5 - 2+y][rand() % 5 - 2+x]&0xFF);
	bioo = (sim->pmap[y+ry][x+rx]&0xFF);
	
	if (ry == 1 && tempe > 363.15f)
		parts[i].vy = -1;
	else if (tempe > 363.15f)
		parts[i].vy = 1;
	if (bio == PT_WATR && bioo == PT_AFRZ) 
	{
		if (ry == 1) 
		{
			sim->part_change_type(i,x,y,PT_BDSL);
		}
		else
		{
			sim->part_change_type(i,x,y,PT_OIL);
		}
		return 1;
	}
	else if (bio == PT_WATR && bioo == PT_BRMT && tempe > 290.15f && tempe < 330.15f) 
	{
		sim->part_change_type(i,x,y,PT_SOAP);
		sim->kill_part(sim->pmap[y+ry][x+rx]>>8);
	}
	return 0;
}


Element_COIL::~Element_COIL() {}
