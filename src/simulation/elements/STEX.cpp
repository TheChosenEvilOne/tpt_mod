#include "simulation/Elements.h"
//#TPT-Directive ElementClass Element_STEX PT_STEX 192
Element_STEX::Element_STEX()
{
	Identifier = "DEFAULT_PT_STEX";
	Name = "STEX";
	Colour = PIXPACK(0xD080E0);
	MenuVisible = 1;
	MenuSection = SC_LIQUID;
	Enabled = 1;

	Advection = 0.6f;
	AirDrag = 0.01f * CFDS;
	AirLoss = 0.98f;
	Loss = 0.95f;
	Collision = 0.0f;
	Gravity = 0.1f;
	Diffusion = 0.00f;
	HotAir = 0.000f  * CFDS;
	Falldown = 2;

	Flammable = 0;
	Explosive = 0;
	Meltable = 0;
	Hardness = 20;

	Weight = 35;

	Temperature = R_TEMP-2.0f  +273.15f;
	HeatConduct = 29;		
	Description = "Sticky C-4.";

	Properties = TYPE_LIQUID | PROP_NEUTPENETRATE | PROP_LIFE_DEC;

	LowPressure = IPL;
	LowPressureTransition = NT;
	HighPressure = IPH;
	HighPressureTransition = NT;
	LowTemperature = ITL;
	LowTemperatureTransition = NT;
	HighTemperature = ITH;
	HighTemperatureTransition = NT;

	Update = &Element_STEX::update;
}

//#TPT-Directive ElementHeader Element_STEX static int update(UPDATE_FUNC_ARGS)
int Element_STEX::update(UPDATE_FUNC_ARGS)
{
	int r, rx, ry;
	for (rx=-2; rx<3; rx++)
		for (ry=-2; ry<3; ry++)
			if (BOUNDS_CHECK && (rx || ry))
			{
				r = pmap[y+ry][x+rx];
				if (!r)
					continue;
				else if ((r&0xFF) == PT_FIRE || (r&0xFF) == PT_PLSM || sim->pv[(y/CELL)+ry][(x/CELL)+rx] > 1.32f) {
					parts[i].pavg[0] = 1;
					parts[i].life = rand() % 20 +1;
					continue;
				}
				float dx, dy;
				dx = parts[i].x - parts[r>>8].x;
				dy = parts[i].y - parts[r>>8].y;

				if ((dx*dx + dy*dy)>1.5 && (fabs((float)rx)<4 && fabs((float)ry)<4) && (r&0xFF) != PT_STEX)
				{
					float per, nd;
					nd = dx*dx + dy*dy - 0.5;
					per = 10*(nd/(dx*dx + dy*dy + nd) - 0.5);
					dx *= per; 
					dy *= per;
					parts[i].vx += dx;
					parts[i].vy += dy;
				}
			}
	if (parts[i].pavg[0] == 1 && parts[i].life < 2) 
	{
		for (int sniff = 0;sniff < rand() % 20 + 1;sniff++)
			sim->pv[(y/CELL)][(x/CELL)] += 1.2f;
			sim->create_part(-1, x+rand()%7-3, y+rand()%7-3,PT_PLSM);
		sim->part_change_type(i,x,y,PT_PLSM);
		return 1; 
	}
	return 0;
}

Element_STEX::~Element_STEX() {}
