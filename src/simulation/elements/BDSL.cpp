#include "simulation/Elements.h"
//#TPT-Directive ElementClass Element_BDSL PT_BDSL 193
Element_BDSL::Element_BDSL()
{
	Identifier = "DEFAULT_PT_BDSL";
	Name = "BDSL";
	Colour = PIXPACK(0x331000);
	MenuVisible = 1;
	MenuSection = SC_LIQUID;
	Enabled = 1;

	Advection = 0.8f;
	AirDrag = 0.02f * CFDS;
	AirLoss = 0.94f;
	Loss = 0.80f;
	Collision = 0.0f;
	Gravity = 0.1f;
	Diffusion = 0.0f;
	HotAir = 0.0f	* CFDS;
	Falldown = 2;

	Flammable = 0;
	Explosive = 0;
	Meltable = 0;
	Hardness = 5;

	Weight = 12;

	Temperature = R_TEMP+295.15f;
	HeatConduct = 99;
	Description = "Bio Diesel. Made with COIL.";

	Properties = TYPE_LIQUID;

	LowPressure = IPL;
	LowPressureTransition = NT;
	HighPressure = 5.0f;
	HighPressureTransition = PT_FIRE;
	LowTemperature = ITL;
	LowTemperatureTransition = NT;
	HighTemperature = 403.15f;
	HighTemperatureTransition = PT_FIRE;

	Update = &Element_BDSL::update;
}

//#TPT-Directive ElementHeader Element_BDSL static int update(UPDATE_FUNC_ARGS)
int Element_BDSL::update(UPDATE_FUNC_ARGS)
{
	int fc,of;
	fc = (sim->pmap[y+rand() % 3 - 1][x+rand() % 3 - 1]&0xFF);
	of = (sim->pmap[y+rand() % 3 - 1][x+rand() % 3 - 1]&0xFF);
	if (fc == PT_COIL)
		parts[i].life = 2;
	else if (fc == PT_ACEL && of == PT_DESL) 
		sim->part_change_type(i,x,y,PT_DESL);
	else if (of == PT_SPRK && of == PT_LIGH) 
		sim->part_change_type(i,x,y,PT_FIRE);
	return 0;
}

Element_BDSL::~Element_BDSL() {}
