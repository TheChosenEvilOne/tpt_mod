#include "simulation/Elements.h"
//#TPT-Directive ElementClass Element_GBSH PT_GBSH 196
Element_GBSH::Element_GBSH()
{
	Identifier = "DEFAULT_PT_GBSH";
	Name = "GBSH";
	Colour = PIXPACK(0x242424);
	MenuVisible = 1;
	MenuSection = SC_NUCLEAR;
	Enabled = 1;

	Advection = 0.7f;
	AirDrag = 0.02f * CFDS;
	AirLoss = 0.96f;
	Loss = 0.80f;
	Collision = 0.0f;
	Gravity = 0.2f;
	Diffusion = 0.00f;
	HotAir = 0.000f	* CFDS;
	Falldown = 1;

	Flammable = 0;
	Explosive = 0;
	Meltable = 0;
	Hardness = 30;

	Weight = 85;

	Temperature = R_TEMP+0.0f	+273.15f;
	HeatConduct = 70;
	Description = "GEL BURN SLOW & HOT";

	Properties = TYPE_PART|PROP_LIFE_DEC;

	LowPressure = IPL;
	LowPressureTransition = NT;
	HighPressure = IPH;
	HighPressureTransition = NT;
	LowTemperature = ITL;
	LowTemperatureTransition = NT;
	HighTemperature = ITH;
	HighTemperatureTransition = NT;

	Update = &Element_GBSH::update;
}

//#TPT-Directive ElementHeader Element_GBSH static int update(UPDATE_FUNC_ARGS)
int Element_GBSH::update(UPDATE_FUNC_ARGS)
{
	int r,ry,rx,ty,tx,lx,ly;
	for (rx=-5; rx<6; rx++)
		for (ry=-5; ry<6; ry++)
			if (BOUNDS_CHECK && (rx || ry))
			{
				r = sim->pmap[y+ry][x+rx];
				if (!r)
					continue;
				if (((r&0xFF) == PT_FIRE || (r&0xFF) == PT_PLSM || (r&0xFF) == PT_SPRK || sim->pv[(y/CELL)][(x/CELL)]>17.f) && parts[i].life < 1) 
				{
					parts[i].life = rand() % 1000 + 91;
				}
				if ((r&0xFF) != PT_GBSH && rand() % 20 + 1)
				{
					tx = parts[r>>8].x;
					ty = parts[r>>8].y;
					parts[i].vx = tx-x;
					parts[i].vy = ty-y;
					parts[i].vx *= 1.2;
					parts[i].vy *= 1.2;
				}
			}
	if (parts[i].life < 2 && parts[i].life != 0)
	{
		parts[i].life = 100;
		sim->part_change_type(i,x,y,PT_PLSM);
		return 1;
	}
	else if (parts[i].life > 1)
	{
		sim->pv[(y/CELL)][(x/CELL)] += rand() % 3;
		parts[i].temp += rand() % 100;
		lx = x + rand() % 3 - 1;
		ly = y + rand() % 3 - 1;
		if ((sim->pmap[ly][lx] & 0xFF) == PT_NONE)
			sim->create_part(-1, lx, ly, PT_PLSM);
	}
	return 0;
}
Element_GBSH::~Element_GBSH(){}
